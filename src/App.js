// To start the app type npom start on gitbash on main folder
import {useState, useEffect} from 'react';
import './App.css';
import { Container } from 'react-bootstrap/';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import AppNavbar from './components/AppNavbar.js'
import CourseView from './components/CourseView.js'
import Home from './pages/Home.js';
import Courses from  './pages/Courses.js'
import Login from  './pages/Login.js'
import Register from  './pages/Register.js'
import NotFound from  './pages/ErrorPage.js'
import Logout from  './pages/Logout.js'

import {UserProvider} from './UserContext.js';



// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page



function App() {
  
  const [user, setUser] = useState({
    //email: localStorage.getItem('email')
    id: null,
    isAdmin: null,
    email: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })


  return (
    //we are mounting our components to prepare for output for rendering
    //para di magagawan ng space ang components kailangan ng parent
    //Fragment "<> and </> to act as parents of components"
    <UserProvider value={{user, setUser, unsetUser}}>
      <>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route exact path = "/" element = {<Home/>}/>
              <Route exact path = "/courses" element = {<Courses/>}/>
              <Route exact path = "/login" element = {<Login/>}/>
              <Route exact path = "/register" element = {<Register/>}/>
              <Route exact path = "/courseView/:courseId" element = {<CourseView/>}/>
              <Route exact path = "/logout" element = {<Logout/>}/>
              <Route path = "*" element={<NotFound/>} />
            </Routes>
          </Container>
        </Router>
      </>
    </UserProvider>
  );
}

export default App;
