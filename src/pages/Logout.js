import {useContext, useEffect} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function Logout(){

	
	// use the USerCOntext object and destructure it to access the "user" state and "unsetUser" function from the context provider
	const {unsetUser, setUser} = useContext(UserContext);

	// clear the localstorage of the user's information
	unsetUser();

	//localStorage.clear();

	useEffect(()=> {
		// set the user state back to its original value 
		setUser({email: null})
	})

	return(

			<Navigate to="/login"/>

		)
}