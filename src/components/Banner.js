//Destructuring components/modules for cleaner codebase
import {Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom';
export default function Banner(){
	return(
		<Row>
			<Col className="p-4 text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opprotunities for everyone, everywhere</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
		)
}

/*export default function Banner({Data}){
	const {title, content, destination, label}
	return(
			<Row>
				<Col className="p-4 text-center">
					<h1>{title}</h1>
					<p>{content}</p>
					<Button variant="primary" as={Link}>{label}</Button>
				</Col>
			</Row>
		)

}*/



