import {Card, Button} from 'react-bootstrap';
import {useState} from 'react';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProps}){
	//checks to see if the data was received
	console.log(courseProps);
	console.log(typeof courseProps);
	
	// destructuring the data to avoid notation
	const {name, description, price, _id} = courseProps;

	// 3 Hooks in React
	// 1. useState
	// 2. useEffect
	// 3. useContext


	// Use the useState hook for the component to be able to store state
	// States are used to keep track of information related to individual components

	// syntax -> const [getter, setter] = userState(initialGetterValue)

	const [count, setCount] = useState(0);
		
	let seatCount = 30;
	/*function enroll(){
		if(count==30){
			alert("No more seats")
		} else {
			setCount(count +1);
		}
			
	}*/

	return(
        <Card>
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Card.Text>Enrollees: {count}</Card.Text>
	            <Card.Text>Seats available: {seatCount-count}</Card.Text>
	            <Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>
	        </Card.Body>
	    </Card>
		)
}